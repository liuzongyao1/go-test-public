#FROM 192.168.34.233:60080/alaudaorg/qaimages:helloworld
FROM 172.31.30.27:60080/automation/qaimages:helloworld
LABEL Version="1.1.90790798"
ENV VERSION=1.0.7
COPY a.sh /
CMD echo $VERSION && sleep 60
